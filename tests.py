import unittest

import redis
from redis_dict import RedisDict


class RedisMapTestCase(unittest.TestCase):
    """In order to run this test case you have to start redis server locally"""

    def setUp(self):
        self.connection = redis.Redis()
        self.connection.delete('testmap')
        self.redis_dict = RedisDict('testmap', connection=self.connection)
        self.redis_dict.clear()

    def tearDown(self):
        self.connection.delete('testmap')

    def test_getitem(self):
        self.redis_dict['key'] = 1
        self.assertEqual(self.redis_dict['key'], 1)

    def test_clear(self):
        self.redis_dict['a'] = 1
        self.redis_dict['b'] = 2
        self.redis_dict['c'] = 3
        self.redis_dict.clear()
        self.assertEqual(len(self.redis_dict), 0)

    def test_getitem_raises_exception_for_non_existing_key(self):
        self.assertRaises(KeyError, lambda: self.redis_dict['non existing key'])

    def test_delete_key(self):
        self.redis_dict['key'] = 1
        del self.redis_dict['key']
        self.assertFalse('key' in self.redis_dict)

    def test_values(self):
        self.redis_dict['key'] = 1
        self.assertEqual(self.redis_dict.values(), [1])

    def test_get_method_returns_default_value_for_non_existing_key(self):
        self.assertEqual(self.redis_dict.get('non existing key'), None)
        self.assertEqual(self.redis_dict.get('non existing key', 2), 2)

    def test_update_value_raises_exception_when_dict_changed(self):
        self.redis_dict['one'] = 1
        self.redis_dict['two'] = 2
        intersect = lambda c: self.redis_dict.__setitem__('two', 'two')
        self.assertRaises(redis.WatchError, lambda: self.redis_dict.update_value('one', intersect))

    def test_update_value_retries(self):
        self.redis_dict['one'] = 1
        self.redis_dict['two'] = 2
        class call_counter: count = 0
        def intersect(c):
            self.redis_dict.__setitem__('two', 'two')
            call_counter.count += 1
        self.assertRaises(redis.WatchError, lambda: self.redis_dict.update_value('one', intersect, retry=3, interval=0))
        self.assertEqual(call_counter.count, 4)

    def test_update_value(self):
        self.redis_dict['one'] = 1
        self.redis_dict['two'] = 2
        self.redis_dict.update_value('one', lambda v: v+100)
        self.assertEqual(self.redis_dict['one'], 101)

    def test_update_value_method_on_mutable_object(self):
        self.redis_dict['dict'] = {'a': 1}
        self.redis_dict.update_value('dict', lambda cv: dict(cv, a='A'))
        self.assertEqual(self.redis_dict['dict'], {'a': 'A'})

    def test_items_method(self):
        self.redis_dict['one'] = 1
        self.redis_dict['two'] = 2
        self.assertEqual(set([('one', 1), ('two', 2)]), set(self.redis_dict.items()))

    def test_update_value_method_raises_exception_for_non_existing_key(self):
        self.redis_dict['one'] = 1
        self.assertRaises(KeyError, lambda: self.redis_dict.update_value('nonexisting', lambda: None))

    def test_update_value_method_uses_default_value_for_non_existing_key(self):
        self.redis_dict['one'] = 1
        self.redis_dict.update_value('two', lambda v: v, default='default')
        self.assertEqual(set([('one', 1), ('two', 'default')]), set(self.redis_dict.items()))

    def test_update_value_method_deletes_key_when_MISSING_object_is_returned(self):
        self.redis_dict['one'] = 1
        self.redis_dict.update_value('one', lambda v: RedisDict.MISSING, default='default')
        self.assertEqual(set([]), set(self.redis_dict.items()))

    def test_update_method_drops_missing_keys(self):
        self.redis_dict['one'] = 1
        self.redis_dict.update(lambda _: {'two': 2})
        self.assertTrue('one' not in self.redis_dict.keys())

    def test_update_method_adds_new_values_correctly(self):
        self.redis_dict['one'] = 1
        self.redis_dict.update(lambda _: {'two': 2})
        self.assertTrue('two' in self.redis_dict.keys())
        self.assertEqual(self.redis_dict['two'], 2)

    def test_update_method_deletes_hash_when_MISSING_object_is_returned(self):
        self.redis_dict['one'] = 1
        self.redis_dict.update(lambda d: RedisDict.MISSING)
        self.assertEqual([], self.connection.keys('one'))
