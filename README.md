redis-dict
=============

Really tiny wrapper around redis hash which serializes values (by default it uses pickle).

# USAGE

## KEYS
`RedisDict` accepts only `str` instances as keys - it is impossible relly on Python `__hash__` function as its value can differ on different machines (for example `hash(None)` is derived from `None` object `id`).


## UPDATING VALUES

### `RedisDict.update_value` and `RedisDict.update`

If you store mutable objects and want to update them you have to be careful:

* you have to explicite overwrite value in RedisDict instance:

        :::python
            >>> import redis_dict, redis
            >>> connection = redis.Redis()
            >>> rd = redis_dict.RedisDict('new-dict', connection)
            >>> rd['a'] = {'key': 'value'}
            >>> a = rd['a']
            >>> # this call doesn't affects redis state
            >>> a['key'] = 'new value'
            >>> print rd['a']['key']
            value
            >>> # you have to explicite override
            >>> rd['a'] = a
            >>> print rd['a']['key']
            new value

* such an update is not atomic operation - prefered way to do it is to use `update_value` or `update` - both methods use redis transactions (`WATCH` and `MULTI`) to ensure atomicity of operation:

        :::python
            >>> import redis_dict, redis
            >>> connection = redis.Redis()
            >>> rd = redis_dict.RedisDict('new-dict', connection)
            >>> rd['a'] = {'key': 'value'}
            >>> # updater receives current value and should return new value
            >>> rd.update_value('a', lambda curr_value: dict(curr_value, key='new value'))
            >>> print rd['a']['key']
            new value
