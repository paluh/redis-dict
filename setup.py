try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='redis-dict',
    author='Tomasz Rybarczyk',
    author_email='paluho@gmail.com',
    version='2013.10.1',
    license='BSD',
    url='http://bitbucket.org/paluh/redis-dict',
    py_modules=['redis_dict'],
    description='Tiny wrapper around redis hash',
    zip_safe=False,
    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python'
    ],
    requires=['redis (>=2.7.1)'],
)
